package com.ssm.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ssm.entity.Emp;
import com.ssm.service.DeptService;
import com.ssm.service.EmpService;
import com.ssm.util.ConstantUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.List;

@Controller
public class EmpController {

    @Resource
    private EmpService empService;
    @Resource
    private DeptService deptService;

    @RequestMapping("/insertEmp")
    public String insertEmp(Emp emp){
        empService.insertEmp(emp);
        return "redirect:/findEmp.do";
    }

    @RequestMapping("/updateEmp")
    public String updateEmp(Emp emp){
        empService.updateEmp(emp);
        return "redirect:/findEmp.do";
    }

    @RequestMapping("/deleteEmp")
    public String deleteEmp(Integer[] ids){
        empService.deleteEmp(ids);
        return "redirect:/findEmp.do";
    }

    @RequestMapping("/findEmp")
    public String findEmp(@RequestParam(value = "page",required = false,defaultValue = "1") Integer page,
                          Model model){
        PageHelper.startPage(page, ConstantUtil.PAGESIZE);
        List<Emp> list = empService.findEmp();
        PageInfo<Emp> info = new PageInfo<Emp>(list);
        model.addAttribute("info", info);
        return "emp/emp_find";
    }

    @RequestMapping("/showInsertEmp")
    public String showInsertEmp(Model model){
        model.addAttribute("deptList", deptService.findDept());
        return "emp/emp_insert";
    }

    @RequestMapping("/showUpdateEmp")
    public String showUpdateEmp(Integer empno, Model model){
        model.addAttribute("emp", empService.findEmpById(empno));
        model.addAttribute("deptList", deptService.findDept());
        return "emp/emp_update";
    }
}
