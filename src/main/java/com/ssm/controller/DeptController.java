package com.ssm.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ssm.entity.Dept;
import com.ssm.service.DeptService;
import com.ssm.util.ConstantUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.List;

@Controller
public class DeptController {

    @Resource
    private DeptService deptService;

    @RequestMapping("/insertDept")
    public String insertDept(Dept dept){
        deptService.insertDept(dept);
        return "redirect:/findDept.do";
    }

    @RequestMapping("/updateDept")
    public String updateDept(Dept dept){
        deptService.updateDept(dept);
        return "redirect:/findDept.do";
    }

    @RequestMapping("/deleteDept")
    public String deleteDept(Integer[] ids){
        deptService.deleteDept(ids);
        return "redirect:/findDept.do";
    }

    @RequestMapping("/findDept")
    public String findDept(@RequestParam(value = "page",required = false,defaultValue = "1") Integer page,
                            Model model){
        PageHelper.startPage(page, ConstantUtil.PAGESIZE);
        List<Dept> list = deptService.findDept();
        PageInfo<Dept> info = new PageInfo<Dept>(list);
        model.addAttribute("info", info);
        return "dept/dept_find";
    }

    @RequestMapping("/showUpdateDept")
    public String showUpdateDept(Integer deptno, Model model){
        model.addAttribute("dept", deptService.findDeptById(deptno));
        return "dept/dept_update";
    }

    /**
     * 解决安全目录下无法直接打开jsp的问题
     */
    @RequestMapping("/showInsertDept")
    public String showInsertDept(){
        return "dept/dept_insert";
    }

}
