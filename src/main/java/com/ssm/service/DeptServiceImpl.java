package com.ssm.service;

import com.ssm.dao.DeptDao;
import com.ssm.entity.Dept;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service("deptService")
public class DeptServiceImpl implements DeptService {

    @Resource
    private DeptDao deptDao;
    @Override
    public void insertDept(Dept dept) {
        deptDao.insertDept(dept);
    }

    @Override
    public void updateDept(Dept dept) {
        deptDao.updateDept(dept);
    }

    @Override
    public void deleteDept(Integer[] ids) {
        deptDao.deleteDept(ids);
    }

    @Override
    public Dept findDeptById(Integer deptno) {
        return deptDao.findDeptById(deptno);
    }

    @Override
    public List<Dept> findDept() {
        return deptDao.findDept();
    }
}
