package com.ssm.service;

import com.ssm.entity.Emp;

import java.util.List;

public interface EmpService {
    public void insertEmp(Emp emp);
    public void updateEmp(Emp emp);
    public void deleteEmp(Integer[] ids);
    public Emp findEmpById(Integer empno);
    public List<Emp> findEmp();
}
