package com.ssm.service;

import com.ssm.dao.EmpDao;
import com.ssm.entity.Emp;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service("empService")
public class EmpServiceImpl implements EmpService {

    @Resource
    private EmpDao empDao;

    @Override
    public void insertEmp(Emp emp) {
        empDao.insertEmp(emp);
    }

    @Override
    public void updateEmp(Emp emp) {
        empDao.updateEmp(emp);
    }

    @Override
    public void deleteEmp(Integer[] ids) {
        empDao.deleteEmp(ids);
    }

    @Override
    public Emp findEmpById(Integer empno) {
        return empDao.findEmpById(empno);
    }

    @Override
    public List<Emp> findEmp() {
        return empDao.findEmp();
    }
}
