package com.ssm.dao;

import com.ssm.entity.Emp;

import java.util.List;

public interface EmpDao {
    public void insertEmp(Emp emp);
    public void updateEmp(Emp emp);
    public void deleteEmp(Integer[] ids);
    public Emp findEmpById(Integer empno);
    public List<Emp> findEmp();
}
