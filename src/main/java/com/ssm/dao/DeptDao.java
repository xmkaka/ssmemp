package com.ssm.dao;

import com.ssm.entity.Dept;

import java.util.List;

public interface DeptDao {
    public void insertDept(Dept dept);
    public void updateDept(Dept dept);
    public void deleteDept(Integer[] ids);
    public Dept findDeptById(Integer deptno);
    public List<Dept> findDept();
}
