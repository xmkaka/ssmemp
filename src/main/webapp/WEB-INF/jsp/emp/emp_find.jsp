<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <base href="<%=request.getContextPath()%>/">
    <title>Title</title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" />
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="js/commons.js"></script>
    <style type="text/css">
        .tab {height: 440px;}
        .table th {text-align: center;}
        .page {text-align: right;}
    </style>
    <script type="text/javascript">
        function show(){
            var id_box = $(":checkbox[name='ids']:checked");
            if(id_box.length==0){
                alert("请选择一条要修改的数据");
            }else if(id_box.length>1){
                alert("只能修改一条数据");
            }else{
                location.href = "showUpdateEmp.do?empno="+id_box.val();
            }
        }
        function del(){
            var id_box = $(":checkbox[name='ids']:checked");
            if(id_box.length==0){
                alert("请选择要删除的数据");
            }else{
                if(confirm("确定要删除吗?"))
                    $("#delForm")[0].submit();
            }
        }
    </script>
</head>
<body>
<div class="container-fluid">
    <div style="margin-bottom: 5px;">
        <a class="btn btn-default" href="showInsertEmp.do">添加</a>
        <a class="btn btn-default" href="javascript:show();">修改</a>
        <a class="btn btn-default" href="javascript:del()">删除</a>
    </div>
    <div class="tab">
        <form id="delForm" action="deleteEmp.do" method="post">
        <table class="table table-striped table-bordered table-hover table-condensed">
            <thead>
            <tr>
                <th width="5%"><input type="checkbox" id="all"></th>
                <th width="15%">员工姓名</th>
                <th width="10%">岗位</th>
                <th width="10%">管理者</th>
                <th width="20%">入职日期</th>
                <th width="10%">工资</th>
                <th width="10%">奖金</th>
                <th width="10%">所属部门</th>
                <th width="10%">部门位置</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${info.list }" var="e">
                <tr>
                    <td><input type="checkbox" name="ids" value="${e.empno}"></td>
                    <td>${e.ename }</td>
                    <td>${e.job }</td>
                    <td>${e.mgr }</td>
                    <td><fmt:formatDate pattern="yyyy-MM-dd" value="${e.hiredate }"/></td>
                    <td>${e.sal }</td>
                    <td>${e.comm }</td>
                    <td>${e.dept.dname }</td>
                    <td>${e.dept.loc }</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        </form>
    </div>
    <div class="page">
        <ul class="pagination">
            <li><a href="findEmp.do"><span>首页</span></a></li>
            <c:choose>
                <c:when test="${info.isFirstPage }">
                    <li class="disabled"><span>上一页</span></li>
                </c:when>
                <c:otherwise>
                    <li><a href="findEmp.do?page=${info.prePage }"><span>上一页</span></a></li>
                </c:otherwise>
            </c:choose>
            <c:choose>
                <c:when test="${info.isLastPage }">
                    <li class="disabled"><span>下一页</span></li>
                </c:when>
                <c:otherwise>
                    <li><a href="findEmp.do?page=${info.nextPage }"><span>下一页</span></a></li>
                </c:otherwise>
            </c:choose>
            <li><a href="findEmp.do?page=${info.pages }"><span>末页</span></a></li>
        </ul>
    </div>
</div>
</body>
</html>