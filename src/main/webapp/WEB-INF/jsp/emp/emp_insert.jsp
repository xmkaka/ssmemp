<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <base href="<%=request.getContextPath()%>/">
    <title>Title</title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" />
    <style type="text/css">
        form {width: 400px;margin: 0 auto;}
        .buttons {text-align: right;}
        .buttons .btn {width: 100px;}
    </style>
</head>
<body>
<div class="container">
    <div class="page-header">
        <h2>员工新增</h2>
    </div>
    <form action="insertEmp.do" method="post">
        <div class="form-group">
            <div class="input-group">
                <div class="input-group-addon">员工姓名</div>
                <input class="form-control" type="text" name="ename"/>
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <div class="input-group-addon">岗&emsp;&emsp;位</div>
                <input class="form-control" type="text" name="job"/>
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <div class="input-group-addon">管&nbsp;理&nbsp;者</div>
                <input class="form-control" type="text" name="mgr"/>
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <div class="input-group-addon">入职日期</div>
                <input class="form-control" type="date" name="hiredate"/>
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <div class="input-group-addon">工&emsp;&emsp;资</div>
                <input class="form-control" type="text" name="sal"/>
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <div class="input-group-addon">奖&emsp;&emsp;金</div>
                <input class="form-control" type="text" name="comm"/>
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <div class="input-group-addon">部&emsp;&emsp;门</div>
                <select class="form-control" name="dept.deptno">
                    <c:forEach items="${deptList }" var="d">
                        <option value="${d.deptno }">${d.dname }</option>
                    </c:forEach>
                </select>
            </div>
        </div>
        <div class="buttons">
            <button class="btn btn-primary" type="submit">确定</button>
            <button class="btn btn-default" type="reset">取消</button>
        </div>
    </form>
</div>
</body>
</html>