<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <base href="<%=request.getContextPath()%>/">
    <title>Title</title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" />
    <style type="text/css">
        form {width: 400px;margin: 0 auto;}
        .buttons {text-align: right;}
        .buttons .btn {width: 100px;}
    </style>
</head>
<body>
<div class="container">
    <div class="page-header">
        <h2>部门新增</h2>
    </div>
    <form action="insertDept.do" method="post">
        <div class="form-group">
            <div class="input-group">
                <div class="input-group-addon">部门名称</div>
                <input class="form-control" type="text" name="dname"/>
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <div class="input-group-addon">部门位置</div>
                <input class="form-control" type="text" name="loc"/>
            </div>
        </div>
        <div class="buttons">
            <button class="btn btn-primary" type="submit">确定</button>
            <button class="btn btn-default" type="reset">取消</button>
        </div>
    </form>
</div>
</body>
</html>