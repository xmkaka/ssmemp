function selectAll() {
    var id_box = $(":checkbox[name='ids']");
    var all_box = $(".all");
    id_box.prop("checked",all_box.prop("checked"));
}
function selectOne() {
    var id_box = $(":checkbox[name='ids']");
    var id_checked_box = $(":checkbox[name='ids']:checked");
    $(".all").prop("checked",id_box.length==id_checked_box.length);
}
$(function () {
    $(":checkbox[name='ids']").click(function () {
        selectOne();
    });
    $(".all").click(function () {
        selectAll();
    });
});